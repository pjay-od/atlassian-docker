#!/bin/bash

echo "Building atlassian-base..." && docker build -t "mswinarski/atlassian-base:1.8" ./base/1.8/
echo "Building atlassian-fisheye 3.7..." && docker build -t "mswinarski/atlassian-fisheye:3.7" ./fisheye/3.7/
echo "Building atlassian-crucible 3.7..." && docker build -t "mswinarski/atlassian-crucible:3.7" ./crucible/3.7/
echo "Building atlassian-fisheye 3.8..." && docker build -t "mswinarski/atlassian-fisheye:3.8" ./fisheye/3.8/
echo "Building atlassian-crucible 3.8..." && docker build -t "mswinarski/atlassian-crucible:3.8" ./crucible/3.8/
echo "Building atlassian-fisheye latest..." && docker build -t "mswinarski/atlassian-fisheye:latest" ./fisheye/3.8/
echo "Building atlassian-crucible latest..." && docker build -t "mswinarski/atlassian-crucible:latest" ./crucible/3.8/



