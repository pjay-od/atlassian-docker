#!/bin/bash

echo "Pushing atlassian-base..." && docker push "mswinarski/atlassian-base"
echo "Pushing atlassian-fisheye..." && docker push "mswinarski/atlassian-fisheye"
echo "Pushing atlassian-crucible..." && docker push "mswinarski/atlassian-crucible"
